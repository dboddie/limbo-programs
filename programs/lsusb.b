# lsusb.b
#
# Written in 2019 by David Boddie <david@boddie.org.uk>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

# Lists USB devices that are accessible from outside the USB driver framework.

implement LsUsb;

include "sys.m";
    sys: Sys;
include "draw.m";
include "usb.m";
    usb: Usb;
    Dev, Usbdev: import usb;

usbbase: con "/dev/usb";
stderr: ref sys->FD;

LsUsb: module
{
    init: fn(ctxt: ref Draw->Context, args: list of string);
};

init(ctxt: ref Draw->Context, args: list of string)
{
    sys = load Sys Sys->PATH;
    usb = load Usb Usb->PATH;

    stderr = sys->fildes(2);

    usb->init();

    # Check for a USB device file.
    fd := sys->open(usbbase, sys->OREAD);
    if (fd == nil) {
        sys->fprint(stderr, "Cannot open: %r\n");
    }

    # Examine each device.
    (n, d) := sys->dirread(fd);

    for (i := 0; i < n; i++) {
        if (d[i].name == "ctl") continue;

        fname := "/dev/usb/" + d[i].name;

        dev := usb->opendev(fname);
        if (dev == nil) {
            #sys->fprint(stderr, "Failed to open %s\n", fname);
            continue;
        }

        devdata := usb->opendevdata(dev, sys->ORDWR);
        if (devdata == nil) {
            #sys->fprint(stderr, "Failed to open %s/data\n", fname);
            continue;
        }

        if (usb->configdev(dev) < 0) {
            #sys->fprint(stderr, "Failed to configure device %x\n", dev);
            continue;
        }

        usbdev := dev.usb;
        if (usbdev == nil) {
            #sys->fprint(stderr, "No Usbdev\n");
            continue;
        }

        sys->print("%s vid=%x did=%x vendor=\"%s\" product=\"%s\" serial=\"%s\"\n",
                   d[i].name, usbdev.vid, usbdev.did, usbdev.vendor,
                   usbdev.product, usbdev.serial);
    }
}
