# Modified version of hellofs.b by Pete Elmore: http://static.debu.gs/hellofs.b
# Simplified by David Boddie as a learning experience.

implement SimpleFS;

include "sys.m"; sys: Sys;
    fprint, fildes: import sys;
    OREAD: import Sys;

include "styx.m"; styx: Styx;
    Tmsg: import styx;

include "styxservers.m";
    styxservers: Styxservers;
    Styxserver, Navigator, Navop, Enotfound, Enotdir: import styxservers;

include "draw.m";

SimpleFS: module {
    init: fn(ctxt: ref Draw->Context, args: list of string);
};

Qroot, Qhello, Qmax: con iota;
tab := array[] of {
    (Qroot, ".", Sys->DMDIR|8r555),
    (Qhello, "hello", 8r444),
};

user: string;
greeting: con "Hello, World!\n";

init(nil: ref Draw->Context, nil: list of string)
{
    sys = load Sys Sys->PATH;
    styx = checkload(load Styx Styx->PATH, Styx->PATH);
    styxservers = checkload(load Styxservers Styxservers->PATH, Styxservers->PATH);

    # Make the current user the file owner, falling back to a default user.
    user = readfile("/dev/user");
    if(user == nil)
        user = "inferno";

    # Initialise the modules before doing anything else.
    styx->init();
    styxservers->init(styx);

    # Create a channel and pass it to the navigator thread before starting it.
    navch := chan of ref Navop;
    spawn navigator(navch);

    # Create a navigator to handle communication with the navigator thread.
    nav := Navigator.new(navch);
    (tc, srv) := Styxserver.new(fildes(0), nav, big Qroot);
    servloop(tc, srv);
}

navigator(c: chan of ref Navop)
{
    loop: while (1) {
        navop := <- c;
        pick op := navop {
        Stat =>
            op.reply <-= (dir(int op.path), nil);
            
        Walk =>
            if (op.name == "..") {
                op.reply <-= (dir(Qroot), nil);
                continue loop;
            }
            case int op.path & 16rff {
            Qroot =>
                for(i := 1; i < Qmax; i++) {
                    if(tab[i].t1 == op.name) {
                        op.reply <-= (dir(i), nil);
                        # Break out to the outer while loop.
                        continue loop;
                    }
                }
                op.reply <-= (nil, Enotfound);
            * =>
                op.reply <-= (nil, Enotdir);
            }
            
        Readdir =>
            for(i := 0; i < op.count && i + op.offset < (len tab) - 1; i++) {
                op.reply <-= (dir(Qroot+1+i+op.offset), nil);
            }
            op.reply <-= (nil, nil);
        }
    }
}

servloop(tc: chan of ref Tmsg, srv: ref Styxserver)
{
    loop: while((tmsg := <-tc) != nil) {
        #sys->fprint(sys->fildes(2), "%s\n", tmsg.text());
        pick tm := tmsg {
        Open =>
            srv.default(tm);

        Read =>
            f := srv.getfid(tm.fid);
            if(f.qtype & Sys->QTDIR) {
                srv.default(tm);
                continue loop;
            }
            case int f.path {
            Qhello =>
                srv.reply(styxservers->readstr(tm, greeting));
            * =>
                srv.default(tm);
            }
        * =>
            srv.default(tmsg);
        }
    }
}

# Here are a few utility functions, not particularly required reading.

# Reads a file (or the first chunk if its contents don't fit into one read())
readfile(f: string): string
{
    fd := sys->open(f, Sys->OREAD);
    if(fd == nil)
        return nil;
    buf := array[Sys->ATOMICIO] of byte;
    n := sys->read(fd, buf, len buf);
    if(n <= 0)
        return nil;
    return string buf[0:n];
}

# Given a path inside the table, this returns a Sys->Dir representing that path.
dir(path: int): ref Sys->Dir
{
    (nil, name, perm) := tab[path&16rff];
    d := ref sys->zerodir;
    d.name = name;
    d.uid = d.gid = user;
    d.qid.path = big path;
    if(perm & Sys->DMDIR)
        d.qid.qtype = Sys->QTDIR;
    else
        d.qid.qtype = Sys->QTFILE;
    d.mtime = d.atime = 0;
    d.mode = perm;
    if(path == Qhello)
        d.length = big len greeting;
    return d;
}

checkload[T](x: T, p: string): T
{
    if(x == nil) {
        sys->fprint(sys->fildes(2), "simplefs: cannot load %s: %r\n", p);
        raise "fail:error";
    }
    return x;
}
